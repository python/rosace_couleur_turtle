from turtle import *

def polygone(taille, cotes):
    for i in range(0,cotes) :
        forward(taille)
        left(360//cotes)
    return

def rosace(taille, cote):
    (rd,gd,bd)=(255, 0, 0)
    color(rd,gd,bd)
    for k in range(0,36) :
        polygone(taille,cote)
        left(10)
        (rd,gd,bd)=(rd-7,gd,bd+7)
        color(rd,gd,bd)
    return

Screen()
colormode(255)
rosace(100,5)